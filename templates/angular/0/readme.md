# Quickstarter Django

#### Start docker
##### Dev
docker-compose up --build
##### Prod
sudo docker-compose -d build

#### Get container terminal
docker ps
docker exec -it django2quickstarter_django_1 sh

#### Clean container
docker rm $(docker ps -aq)
docker rmi $(docker images -q)                   


#### Django
python manage.py makemigrations
python manage.py migrate
