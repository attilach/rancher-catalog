from django.conf.urls import url
from django.urls import path

from myprofile import views
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^edit/$', views.edit, name='profile_edit'),
    url(r'^$', views.profile, name='profile_detail'),

    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),

    path('profiles/<int:pk>', views.UserProfileDetail.as_view()),
    path('profiles/', views.UserProfileList.as_view()),
]
