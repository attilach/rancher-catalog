# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User

from django.db import models
from djmoney.models.fields import MoneyField
from phonenumber_field.modelfields import PhoneNumberField


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="profile", on_delete=models.CASCADE)  # La liaison OneToOne vers le modèle User

    # TODO Uncomment wanted fields

    avatar = models.ImageField(upload_to="avatars/", verbose_name=u'Photo',blank=True, null=True, help_text=u'Les personnes avec une photo ont un plus grand taux de succès')

    # https://github.com/stefanfoulis/django-phonenumber-field
    # append to base.py: PHONENUMBER_DB_FORMAT = 'INTERNATIONAL'
    phone = PhoneNumberField(verbose_name='Téléphone', blank=True, help_text='Votre numéro au format internationale. example: +4122 755 XX XX')

    address = models.CharField(max_length=255, verbose_name='Région', help_text='Exemple: Genève, Suisse | Lausanne, Suisse')

    # https://github.com/django-money/django-money
    # append to base.py: CURRENCIES = ('CHF', 'EUR')
    price = MoneyField(max_digits=10, decimal_places=2, default_currency='CHF')

    description = models.TextField(max_length=2000, verbose_name='Votre parcours', help_text='exemple: Etudiant en mathématique en deuxième année à l\'université de Genève.')

    published = models.BooleanField(default=True, verbose_name='Publier', help_text='Cochez la case ci-dessus si vous souhaitez rendre votre compte visible par les visiteurs')  # Est-ce que le profile est visible publiquement

    # End subscription date
    # enddate = models.DateField(auto_now=False, auto_now_add=False, default=date(2015, 1, 1), verbose_name=u'Date de fin de validité')



    ## OTHER FIELDS

    ### Tutoring

    # level = models.ForeignKey('Level', verbose_name=u'Niveau d\'étude', null=True, help_text=u'Choisissez le niveau d\'étude le plus haut pour lequel vous offrez vos services')


    ### Services

    # services = models.ManyToManyField('Service', verbose_name=u'Prestations fournies')

    def __str__(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)



    # TODO Copy and adapt model

    # class Service(models.Model):
    #     title = models.CharField(max_length=30)
    #
    #     def __unicode__(self):
    #         return self.title
