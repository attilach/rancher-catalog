# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.http import Http404
from django.shortcuts import render, redirect

from rest_framework.permissions import AllowAny

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from myprofile.models import UserProfile
from myprofile.forms import UserProfileEditForm, UserEditForm
from myprofile.serializers import UserSerializer, UserProfileSerializer


def profile(request):
    return render(request, 'myprofile/detail.html')


def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        user_profile_form = UserProfileEditForm(instance=request.user.profile,
                                       data=request.POST,
                                       files=request.FILES)
        if user_form.is_valid() and user_profile_form.is_valid():
            user_form.save()
            user_profile_form.save()
            return redirect("profile_detail")
    else:
        user_form = UserEditForm(instance=request.user)
        user_profile_form = UserProfileEditForm(instance=request.user.profile)

    return render(request,
                  'myprofile/edit.html',
                  {'user_form': user_form,
                   'user_profile_form': user_profile_form})


def create_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        userprofile = UserProfile(user=user)
        userprofile.save()

post_save.connect(create_profile, sender=User)



class UserProfileList(APIView):
    """
    List all users, or create a new user.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):

        qs = UserProfile.objects.all()

        # TODO: Adapt filter -> exemple: /profile/users/?first_name=kevin
        # first_name = self.request.query_params.get('first_name', None)
        # if first_name is not None:
        #     qs = qs.filter(first_name=first_name)

        serializer = UserProfileSerializer(qs, many=True) #
        return Response(serializer.data)


class UserProfileDetail(APIView):
    """
    Retrieve, update or delete a user instance.
    """
    permission_classes = (AllowAny,)

    def get_object(self, pk):
        try:
            return UserProfile.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        userprofile = self.get_object(pk)
        userprofile = UserProfileSerializer(userprofile)
        return Response(userprofile.data)

    def put(self, request, pk, format=None):
        userprofile = self.get_object(pk)
        serializer = UserProfileSerializer(userprofile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserList(APIView):
    """
    List all users, or create a new user.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=None):

        qs = User.objects.all()

        # TODO: Adapt filter -> exemple: /profile/users/?first_name=kevin
        # first_name = self.request.query_params.get('first_name', None)
        # if first_name is not None:
        #     qs = qs.filter(first_name=first_name)

        serializer = UserSerializer(qs, many=True) #
        return Response(serializer.data)


    # TODO: Clean unused  -> exemple: /profile/users/?first_name=kevin

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    """
    Retrieve, update or delete a user instance.
    """
    permission_classes = (AllowAny,)

    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = self.get_object(pk)
        user = UserSerializer(user)
        return Response(user.data)

    # def put(self, request, pk, format=None):
    #     user = self.get_object(pk)
    #     serializer = UserSerializer(user, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    #
    # def delete(self, request, pk, format=None):
    #     user = self.get_object(pk)
    #     user.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)