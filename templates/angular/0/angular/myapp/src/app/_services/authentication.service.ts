﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) { }
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  login(username: string, password: string) {
    // add content type header
    const headers = new HttpHeaders().set("Content-Type", "application/json");

    return this.http.post<any>('http://127.0.0.1:8000/rest-auth/login/', JSON.stringify({ username: username, password: password }), {headers})
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          console.log("GNIAAAAA");
          console.log(user);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.loggedIn.next(true);
        }

        return user;
      });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.loggedIn.next(false);
  }
}
