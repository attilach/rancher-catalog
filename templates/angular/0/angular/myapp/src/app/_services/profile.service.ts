﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserProfile } from "../_models/profile";
import { BACKEND_URL } from "../_others/constants";

@Injectable()
export class UserProfileService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<UserProfile[]>(BACKEND_URL + 'profile/profiles/');
    }

    getById(id: number) {
        return this.http.get<UserProfile>(BACKEND_URL + 'profile/profiles/' + id);
    }

    update(profile: UserProfile) {
        return this.http.put(BACKEND_URL + 'profile/profiles/' + profile.id, profile);
    }
}
