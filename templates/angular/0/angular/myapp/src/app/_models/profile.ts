export class UserProfile {
    id: number;
    phone: string;
    address: string;
    price: string;
    description: string;
    published: string;
}
