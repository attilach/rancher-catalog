﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserProfile } from "../_models/index";
import { UserService } from '../_services/index';
import { UserProfileService } from '../_services/index';
import { ActivatedRoute } from "@angular/router";

@Component({
    moduleId: module.id,
    templateUrl: 'user.component.html',
    styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
    currentUser: User;
    currentProfile: UserProfile;

    constructor(private userService: UserService, private userProfileService: UserProfileService, private route: ActivatedRoute) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
       this.loadProfile();
    }

    private loadProfile() {
      console.log("GNIAAAAAAAAAAAAAAAAAAA");
      console.log(this.currentUser);
        this.userProfileService.getById(this.currentUser.id).subscribe(profiles => { this.currentProfile = profiles; });
    }
}
