﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserProfile } from "../_models/index";
import { UserService } from '../_services/index';
import { UserProfileService } from '../_services/index';
import { ActivatedRoute } from "@angular/router";

@Component({
    moduleId: module.id,
    templateUrl: 'user.component.html',
    styleUrls: ['./user.component.css']
})

export class UserEditComponent implements OnInit {
    currentUser: User;
    currentProfile: UserProfile;


    constructor(private userService: UserService, private userProfileService: UserProfileService, private route: ActivatedRoute) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
      this.route.params.subscribe(params => {
         this.loadProfile(+params['id']);
      });
    }

    private loadProfile(id) {
        this.userProfileService.getById(id).subscribe(profiles => { this.currentProfile = profiles; console.log("testtttttttttttttt"); console.log(this.currentProfile); });
    }
}
