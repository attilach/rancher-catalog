﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserProfile } from "../_models/index";
import { UserService } from '../_services/index';
import { UserProfileService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    profiles: UserProfile[] = [];

    constructor(private userService: UserService, private userProfileService: UserProfileService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllUsers();
        this.loadAllProfiles();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }

    private loadAllProfiles() {
        this.userProfileService.getAll().subscribe(profiles => { this.profiles = profiles; });
    }
}
