﻿import { Component } from '@angular/core';
import { AuthGuard } from './_guards/auth.guard';
import { User } from './_models/user';
import { UserService } from './_services/user.service';
import { BehaviorSubject } from "rxjs";

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent { }
